# frozen_string_literal: true

class TheMovieDbClient
  def search_movie(query:, page:)
    response = connection.get('/3/search/movie', { query:, page: })

    JSON.parse(response.body)
  rescue JSON::ParserError
    { 'status_message' => response.body, 'success' => false }
  rescue Faraday::ConnectionFailed
    { 'status_message' => 'Connection Timeout', 'success' => false }
  end

  private

  def connection
    @connection ||= Faraday.new(
      url: 'https://api.themoviedb.org',
      headers: {
        'Authorization' => "Bearer #{THEMOVIEDB_KEY}",
        'Content-Type' => 'application/json'
      }
    )
  end
end
