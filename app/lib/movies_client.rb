# frozen_string_literal: true

class MoviesClient
  def search(query:, page:)
    CachedQuery.with_advisory_lock("#{page}-#{query}") do
      cached_query = CachedQuery.transaction do
        CachedQuery
          .where(query:, page:)
          .where(created_at: 2.minutes.ago..)
          .first_or_create
      end

      if cached_query.result.present?
        # rubocop:disable Rails/SkipsModelValidations need to perform an atomic update without running validations
        CachedQuery.update_counters(cached_query.id, cache_hit: 1)
        # rubocop:enable Rails/SkipsModelValidations

        cached_query.reload
      else
        result = TheMovieDbClient.new.search_movie(query:, page:)

        cached_query.update(result:)
        cached_query
      end
    end
  end
end
