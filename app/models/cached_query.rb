# frozen_string_literal: true

class CachedQuery < ApplicationRecord
  validates :query, presence: true
  validates :page, presence: true
end
