# frozen_string_literal: true

class SearchController < ApplicationController
  def show
    return if search_param[:query].blank?

    @query = search_param[:query]
    @page = search_param[:page]&.to_i || 1
    @result = MoviesClient.new.search(query: @query, page: @page)
  end

  private

  def search_param
    params.fetch(:search, {}).permit(:query, :page)
  end
end
