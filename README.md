# My Movie Database

A search engine using [the movie db](https://developer.themoviedb.org/reference/search-movie) with rate limiting and caching.

## Getting Started

These instructions will give you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Make sure you have installed all of the following prerequisites on your development machine:

1. Get [asdf-vm](https://asdf-vm.com/#/core-manage-asdf)
2. Install required plugins
    ```bash
    asdf plugin add ruby
    asdf plugin add nodejs
    asdf plugin add yarn
    asdf plugin add postgres
    ```
3. Install tools
   ```bash
   asdf install
   ```

Optional requirement is [overmind](https://github.com/DarthSim/overmind) which will help us running the project.

### Installing

After installing the [Prerequisites](#prerequisites), you need to

1. Install all dependencies
    ```bash
    bundle install
    yarn install
    ```
2. Start your postgres server by running
    ```bash
    postgres
    ```
3. Create the database
    ```bash
    rails db:setup
    ```
4. Finally start your local development server
    ```bash
    bin/dev
    ```

#### Environment variables

In development we're using `.env` file to store keys and secrets. Copy `.env.example` and fill in keys in case you need them.

## Running the tests

### Sample Tests

The project is using RSpec for testing

```bash
rspec
```

### Style test

The project is using Rubocop as a style guide

```bash
rubocop
```

## Deployment

The project is using Heroku, see its documentation on how to deploy new versions. The site is available at https://my-movie-db-e7dc2dc47ab1.herokuapp.com

## Future improvements

* Plug in error reporting tools, like Rollbar or Sentry
* Create a background worker to clean up old cached data periodically (when we don't need them anymore for the metrics)
* End-to-end browser testing - currently we don't have any dynamic feature on the frontend, but it might change as the project grows
* A modal for cards showing extra details
