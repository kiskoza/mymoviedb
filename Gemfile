# frozen_string_literal: true

source 'https://rubygems.org'

ruby '3.2.2'

gem 'rails', '~> 7.0.7'

gem 'pg'
gem 'puma'
gem 'shakapacker'

gem 'bootsnap', require: false
gem 'dotenv-rails'
gem 'faraday'
gem 'with_advisory_lock'

group :development, :test do
  gem 'byebug'
  gem 'faker'
  gem 'rubocop', require: false
  gem 'rubocop-performance', require: false
  gem 'rubocop-rails', require: false
  gem 'rubocop-rspec', require: false
end

group :development do
  gem 'web-console'

  gem 'bundler-audit'
end

group :test do
  gem 'capybara'
  gem 'database_cleaner'
  gem 'factory_bot_rails'
  gem 'rspec-rails'
  gem 'webmock'
end
