# frozen_string_literal: true

RSpec.describe TheMovieDbClient do
  subject { described_class.new.search_movie(query:, page:) }

  let(:query) { Faker::Movie.title }
  let(:page) { Faker::Number.between(from: 1, to: 10) }

  let(:webmock) do
    WebMock
      .stub_request(:get, 'https://api.themoviedb.org/3/search/movie')
      .with(query: WebMock.hash_including(:page, :query))
  end

  context 'when the query is valid' do
    before do
      webmock.to_return do |request|
        {
          status: 200,
          body: {
            page: request.uri.query_values&.dig('page'),
            results: [
              {
                adult: Faker::Boolean.boolean,
                backdrop_path: "/#{Faker::Lorem.characters(number: 27)}.jpg",
                id: Faker::Number.number(digits: 4),
                original_language: 'en',
                overview: Faker::Lorem.paragraph(sentence_count: 5),
                poster_path: "/#{Faker::Lorem.characters(number: 27)}.jpg",
                release_date: Faker::Date.backward(days: 10 * 365),
                title: request.uri.query_values&.dig('query')
              }
            ],
            total_pages: 15,
            total_results: 287
          }.to_json,
          headers: {}
        }
      end
    end

    it { is_expected.to match hash_including('page', 'total_pages', 'total_results', 'results') }
  end

  context 'when requesting a page exceeding the total number of pages' do
    before do
      webmock.to_return do |request|
        {
          status: 200,
          body: {
            page: request.uri.query_values&.dig('page'),
            results: [],
            total_pages: 15,
            total_results: 287
          }.to_json,
          headers: {}
        }
      end
    end

    it { is_expected.to match hash_including('page', 'total_pages', 'total_results', 'results' => []) }
  end

  context 'when requesting a page exceeding the overall pagination limit' do
    let(:page) { 501 }

    before do
      webmock.to_return do |_request|
        {
          status: 200,
          body: {
            success: false,
            status_code: 22,
            status_message: 'Invalid page: Pages start at 1 and max at 500. They are expected to be an integer.'
          }.to_json,
          headers: {}
        }
      end
    end

    it { is_expected.to match hash_including('status_message', 'success' => false) }
  end

  context 'when the movie db has an internal error' do
    let(:error_message) { Faker::Lorem.sentence(word_count: 10) }

    before do
      webmock.to_return do |_request|
        {
          status: 500,
          body: error_message,
          headers: {}
        }
      end
    end

    it 'returns a unified error hash' do
      is_expected.to match hash_including('status_message' => error_message, 'success' => false)
    end
  end

  context 'when the API has a timeout or network error' do
    before do
      webmock.to_timeout
    end

    it 'returns a unified error hash' do
      is_expected.to match hash_including('status_message' => 'Connection Timeout', 'success' => false)
    end
  end
end
