# frozen_string_literal: true

RSpec.describe MoviesClient do
  subject(:search) { described_class.new.search(query:, page:) }

  let(:query) { Faker::Movie.title }
  let(:page) { Faker::Number.between(from: 1, to: 500) }

  let(:result) { { 'results' => [], 'total_pages' => 15 } }

  let(:client_double) { instance_double(TheMovieDbClient) }

  before do
    allow(TheMovieDbClient).to receive(:new).and_return(client_double)
    allow(client_double).to receive(:search_movie).and_return(result)
  end

  context 'when query is not in the database yet' do
    it 'creates an entry for the new query-page pair' do
      expect { search }.to change { CachedQuery.all }
                             .from(be_empty)
                             .to(
                               contain_exactly(
                                 have_attributes(
                                   query:,
                                   page:,
                                   cache_hit: 0,
                                   result: hash_including('results'),
                                   created_at: a_value
                                 )
                               )
                             )
    end

    context 'when there are more concurrent requests' do
      let(:cache_hit) { 0 }
      let(:number_of_concurrent_requests) { ActiveRecord::Base.connection.pool.size + 1 }

      before do
        threads = Array.new(number_of_concurrent_requests) do
          Thread.new do
            ActiveRecord::Base.connection_pool.with_connection do
              described_class.new.search(query:, page:)
            end
          end
        end
        threads.each(&:join)
      end

      it { expect(CachedQuery.all).to contain_exactly(have_attributes(cache_hit: number_of_concurrent_requests - 1)) }
      it { expect(CachedQuery.all.count).to be 1 }
    end
  end

  context 'when the query already got cached in the database' do
    let!(:cached_query) { create(:cached_query, query:, page:, created_at:, cache_hit:) }

    let(:created_at) { 1.second.ago }
    let(:cache_hit) { Faker::Number.number(digits: 1) }

    it 'does not create a new cache entry' do
      expect { search }.not_to change { CachedQuery.all.to_a }.from(contain_exactly(cached_query))
    end

    it 'does not call the API' do
      search
      expect(client_double).not_to have_received(:search_movie)
    end

    it 'increments the cache hit' do
      expect { search }.to change { cached_query.reload.cache_hit }.from(cache_hit).to(cache_hit + 1)
    end

    context 'when there are more concurrent requests' do
      let(:cache_hit) { 0 }
      let(:number_of_concurrent_requests) { ActiveRecord::Base.connection.pool.size + 1 }

      before do
        threads = Array.new(number_of_concurrent_requests) do
          Thread.new do
            ActiveRecord::Base.connection_pool.with_connection do
              described_class.new.search(query:, page:)
            end
          end
        end
        threads.each(&:join)
      end

      it { expect(cached_query.reload.cache_hit).to eql(number_of_concurrent_requests) }
    end

    context 'when the cached version is older than 2 minutes' do
      let(:created_at) { 3.minutes.ago }

      it 'creates a new cache entry' do
        expect { search }.to change { CachedQuery.all.to_a }
                               .from(contain_exactly(cached_query))
                               .to(contain_exactly(cached_query, have_attributes(query:, page:, cache_hit: 0)))
      end
    end
  end
end
