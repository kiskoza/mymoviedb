# frozen_string_literal: true

RSpec.describe 'Search' do
  let(:movies_client_double) { instance_double(MoviesClient) }
  let(:cached_query) { create(:cached_query, cache_hit:) }
  let(:cache_hit) { 0 }

  before do
    allow(MoviesClient).to receive(:new).and_return(movies_client_double)
    allow(movies_client_double).to receive(:search).and_return(cached_query)
  end

  context 'when it has no parameters' do
    it 'renders only the search bar' do
      get root_path

      expect(response).to be_successful

      expect(response.body).to have_field('Give any keywords for a movie')
      expect(response.body).to have_button('Search')

      expect(response.body).to have_no_text('You got the results fresh from the API!')
      expect(response.body).to have_no_text('You got the results from the cache')
      expect(response.body).to have_no_selector('.card')
    end

    it 'does not call the Movies Client' do
      get root_path

      expect(movies_client_double).not_to have_received(:search)
    end
  end

  context 'when it has search parameters' do
    let(:query) { Faker::Lorem.word }
    let(:page) { Faker::Number.number(digits: 1) }

    it 'shows the result and a correct message' do
      get root_path params: { search: { query:, page: } }

      expect(response).to be_successful
      expect(response.body).to have_field('Give any keywords for a movie')
      expect(response.body).to have_button('Search')

      expect(response.body).to have_text('You got the results fresh from the API!')
      expect(response.body).to have_selector('.card')
    end

    context 'when the result is coming from the cache' do
      let(:cache_hit) { 3 }

      it 'shows the result and a correct message' do
        get root_path params: { search: { query:, page: } }

        expect(response).to be_successful
        expect(response.body).to have_field('Give any keywords for a movie')
        expect(response.body).to have_button('Search')

        expect(response.body).to have_text('You got the results from the cache, it\'s the 3rd hit!')
        expect(response.body).to have_selector('.card')
      end
    end

    context 'when the page parameter is missing' do
      it 'converts it to be 1' do
        get root_path, params: { search: { query: } }

        expect(movies_client_double).to have_received(:search).with(query:, page: 1)
      end
    end

    context 'when the page parameter is not a number' do
      let(:page) { 'not-a-number' }

      it 'converts it to be 0' do
        get root_path, params: { search: { query:, page: } }

        expect(movies_client_double).to have_received(:search).with(query:, page: 0)
      end
    end
  end
end
