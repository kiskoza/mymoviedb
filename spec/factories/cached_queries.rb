# frozen_string_literal: true

FactoryBot.define do
  factory :cached_query do
    query { Faker::Movie.title }
    page { Faker::Number.between(from: 1, to: 500) }
    cache_hit { Faker::Number.number(digits: 1) }
    result do
      {
        page:,
        results: [
          {
            adult: Faker::Boolean.boolean,
            backdrop_path: "/#{Faker::Lorem.characters(number: 27)}.jpg",
            id: Faker::Number.number(digits: 4),
            original_language: 'en',
            overview: Faker::Lorem.paragraph(sentence_count: 5),
            poster_path: "/#{Faker::Lorem.characters(number: 27)}.jpg",
            release_date: Faker::Date.backward(days: 10 * 365),
            title: Faker::Movie.title
          }
        ],
        total_pages: 15,
        total_results: 287
      }
    end
  end
end
