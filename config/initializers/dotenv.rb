# frozen_string_literal: true

Dotenv.require_keys(
  %w[
    THEMOVIEDB_KEY
  ]
)
