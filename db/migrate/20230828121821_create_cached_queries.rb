# frozen_string_literal: true

class CreateCachedQueries < ActiveRecord::Migration[7.0]
  def change
    create_table :cached_queries do |t|
      t.string :query, null: false
      t.integer :page, null: false
      t.integer :cache_hit, null: false, default: 0
      t.jsonb :result

      t.timestamps

      t.index %i[created_at query page]
    end
  end
end
